#include "image.h"

#include <stdlib.h>
#include <string.h>


void initialize_image(struct image *img, uint32_t width, uint32_t height, struct pixel *data) {
//    uint64_t size = sizeof(struct pixel) * width * height;
//    img->data = malloc(size);
//    memcpy_s(img->data, size, data, size);
    img->data = data;
    img->width = width;
    img->height = height;
}

void initialize_empty_image(struct image *img, uint32_t width, uint32_t height) {
    uint64_t size = sizeof(struct pixel) * width * height;
    img->data = malloc(size);
    img->width = width;
    img->height = height;
}


void destruct_image(struct image *img) {
    free(img->data);
}

struct pixel *get_pixel(struct image *img, uint64_t x, uint64_t y) {
    return &(img->data[img->width * y + x]);
}
