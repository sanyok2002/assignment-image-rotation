#ifndef ASSIGNMENT_IMAGE_ROTATION_BLUR_H
#define ASSIGNMENT_IMAGE_ROTATION_BLUR_H
#include <stdlib.h>
struct image img_blur(struct image *in, size_t strength);
struct image img_dilate(struct image *in, size_t strength);
struct image img_erode(struct image *in, size_t strength);
#endif //ASSIGNMENT_IMAGE_ROTATION_BLUR_H
