#include "BMP.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>

#include <inttypes.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *img) {
    printf("-> from_bmp()\n");

    struct bmp_header header;
    size_t res = fread(&header, 1, sizeof(struct bmp_header), in);

    if (res != sizeof(struct bmp_header)) return READ_INVALID_HEADER;
    if (header.bfType != 19778) return READ_INVALID_SIGNATURE;

    uint32_t height = header.biHeight, width = header.biWidth;
    printf("width: %" PRIu32 " | height: %" PRIu32 "\n", width, height);

    struct pixel *tmp = malloc(sizeof(struct pixel) * width * height);

    for (uint64_t i = 0; i < height; i++) {
        res = fread(&(tmp[width * i]), 1, sizeof(struct pixel) * width, in);

        if (res != sizeof(struct pixel) * width) {
            free(tmp);
            return READ_INVALID_BITS;
        }
        fseek(in, width % 4, SEEK_CUR);
    }

    initialize_image(img, width, height, tmp);

//    free(tmp);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    printf("<- to_bmp()\n");

    uint32_t width = img->width, height = img->height;
    uint64_t row_size = (3 * width + 3) & (-4);

    struct bmp_header header = {
            .bfType = 19778,
            .bfileSize = 54 + height * row_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    size_t res = fwrite(&header, 1, sizeof(struct bmp_header), out);
    if (res != sizeof(struct bmp_header)) return WRITE_ERROR;

    uint32_t padding = 0;
    for (uint64_t i = 0; i < height; i++) {
        res = fwrite(&(img->data[i * width]), 1, sizeof(struct pixel) * width, out);
        res += fwrite(&padding, 1, width % 4, out);
        if (res != sizeof(struct pixel) * width + width % 4) return WRITE_ERROR;
    }

    return WRITE_OK;
}
