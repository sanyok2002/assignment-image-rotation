#include <stdio.h>
#include <stdlib.h>

#include "BMP.h"
#include "image.h"
#include "rotate.h"
#include "blur.h"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    printf("\n\n===start===\n");

    FILE *input = fopen(argv[1], "rb");
    FILE *blur = fopen("blur.bmp", "wb");
    FILE *dilate = fopen("dilate.bmp", "wb");
    FILE *erode = fopen("erode.bmp", "wb");
    FILE *rotate = fopen("rotate.bmp", "wb");

    if (!input) {
        printf("couldn't find bmp file!");
        return -1;
    }
    if (!blur||!dilate||!erode||!rotate) {
        printf("couldn't create bmp file!");
        return -1;
    }

    struct image input_img;
    from_bmp(input, &input_img);

    printf("blurin\n");

    struct image blur_img = img_blur(&input_img, (size_t) 10);
    to_bmp(blur, &blur_img);

    printf("dilatin\n");

    struct image dilate_img = img_dilate(&input_img, (size_t) 10);
    to_bmp(dilate, &dilate_img);

    printf("erodin\n");

    struct image erode_img = img_erode(&input_img, (size_t) 10);
    to_bmp(erode, &erode_img);

    printf("rotatin\n");

    struct image rotate_img = img_rotate_ang(&input_img, 3.14/4);
    to_bmp(rotate, &rotate_img);

    printf("cleanup x_x\n");
    fclose(input);
    fclose(blur);
    fclose(dilate);
    fclose(erode);
    fclose(rotate);
    destruct_image(&input_img);
    destruct_image(&blur_img);
    destruct_image(&dilate_img);
    destruct_image(&erode_img);
    destruct_image(&rotate_img);
    printf("===end===\n\n\n");
    return 0;
}
