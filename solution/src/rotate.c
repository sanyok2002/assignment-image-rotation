#include "rotate.h"

#include <stdlib.h>
#include <math.h>

struct image img_rotate(struct image *in) {
    struct image out;
    initialize_empty_image(&out, in->height, in->width);

    for (uint64_t i = 0; i < in->width; i++) {
        for (uint64_t j = 0; j < in->height; j++) {
            *(get_pixel(&out, j, i)) = *(get_pixel(in, i, in->height - j - 1));
        }
    }
    return out;
}
struct image img_rotate_ang(struct image *in, double ang) {
    struct image out;
    initialize_empty_image(&out, in->width, in->height);
    double sin_a = sin(ang), cos_a = cos(ang);
    double hw = in->width/2 , hh = in->height/2;

    for (double i = 0; i < out.width; i++) {
        for (double j = 0; j < out.height; j++) {
            double x_off = i-hw;
            double y_off = j-hh;

            uint64_t x = (uint64_t)(x_off * cos_a + y_off * sin_a + hw);
            uint64_t y = (uint64_t)(y_off * cos_a - x_off * sin_a + hh);

            if(x >= 0 && x < in->width && y >= 0 && y < in->height){
                *get_pixel(&out, i, j) = *get_pixel(in, x, y);
            }else {
                *(get_pixel(&out, i, j)) = (struct pixel){0,0,0};
            }
        }
    }

    return out;
}
