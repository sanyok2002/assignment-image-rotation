#include "blur.h"
#include "image.h"
#include <math.h>

struct image img_blur(struct image *in, size_t strength) {
    struct image out;
    initialize_empty_image(&out, in->width, in->height);

    for (uint64_t i = 0; i < in->width; i++) {
        for (uint64_t j = 0; j < in->height; j++) {
            uint64_t si = fmax(((int64_t)i)-(int64_t)strength, 0);
            uint64_t sj = fmax(((int64_t)j)-(int64_t)strength, 0);
            uint64_t ei = fmin(i+strength, in->width);
            uint64_t ej = fmin(j+strength, in->height);

            struct pixel sum = (struct pixel){0,0,0};
            uint64_t sqr = (ei-si)*(ej-sj);
            uint64_t b = 0, g = 0, r = 0;

            for(uint64_t ii = si; ii < ei; ii++){
                for(uint64_t jj = sj; jj < ej; jj++){
                    struct pixel * curr = get_pixel(in, ii, jj);
                    b += ((uint64_t)curr->b);
                    g += ((uint64_t)curr->g);
                    r += ((uint64_t)curr->r);
                }
            }

            sum.b = b / sqr;
            sum.g = g / sqr;
            sum.r = r / sqr;

            *(get_pixel(&out, i, j)) = sum;
        }
    }
    return out;
}

struct image img_dilate(struct image *in, size_t strength) {
    struct image out;
    initialize_empty_image(&out, in->width, in->height);

    for (uint64_t i = 0; i < in->width; i++) {
        for (uint64_t j = 0; j < in->height; j++) {
            uint64_t si = fmax(((int64_t)i)-(int64_t)strength, 0);
            uint64_t sj = fmax(((int64_t)j)-(int64_t)strength, 0);
            uint64_t ei = fmin(i+strength, in->width);
            uint64_t ej = fmin(j+strength, in->height);

            struct pixel max_p = (struct pixel){0,0,0};
            uint64_t max_s = 0;

            for(uint64_t ii = si; ii < ei; ii++){
                for(uint64_t jj = sj; jj < ej; jj++){
                    struct pixel * curr = get_pixel(in, ii, jj);
                    uint64_t sum = curr->b + curr->g + curr->r;
                    if (sum > max_s){
                        max_p = *curr;
                        max_s = sum;
                    }
                }
            }

            *(get_pixel(&out, i, j)) = max_p;
        }
    }
    return out;
}
struct image img_erode(struct image *in, size_t strength) {
    struct image out;
    initialize_empty_image(&out, in->width, in->height);

    for (uint64_t i = 0; i < in->width; i++) {
        for (uint64_t j = 0; j < in->height; j++) {
            uint64_t si = fmax(((int64_t)i)-(int64_t)strength, 0);
            uint64_t sj = fmax(((int64_t)j)-(int64_t)strength, 0);
            uint64_t ei = fmin(i+strength, in->width);
            uint64_t ej = fmin(j+strength, in->height);

            struct pixel min_p = *(get_pixel(in, si, sj));
            uint64_t min_s = min_p.b + min_p.g + min_p.r;

            for(uint64_t ii = si; ii < ei; ii++){
                for(uint64_t jj = sj; jj < ej; jj++){
                    struct pixel * curr = get_pixel(in, ii, jj);
                    uint64_t sum = curr->b + curr->g + curr->r;
                    if (sum < min_s){
                        min_p = *curr;
                        min_s = sum;
                    }
                }
            }

            *(get_pixel(&out, i, j)) = min_p;
        }
    }
    return out;
}
