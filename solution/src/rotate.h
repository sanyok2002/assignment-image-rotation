#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image.h"


struct image img_rotate(struct image *in);
struct image img_rotate_ang(struct image *in, double ang);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
