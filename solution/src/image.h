#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include  <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

void initialize_empty_image(struct image *img, uint32_t width, uint32_t height);

void initialize_image(struct image *img, uint32_t width, uint32_t height, struct pixel *data);

void destruct_image(struct image *img);

struct pixel *get_pixel(struct image *img, uint64_t x, uint64_t y);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
